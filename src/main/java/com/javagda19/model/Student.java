package com.javagda19.model;


import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
@Setter
@Entity
public class Student {
    @Id // primary key
    @GeneratedValue(strategy = GenerationType.AUTO) // auto_increment
    private long id;

    @Column(nullable = false, name = "imionko")
    private String name;

    @Column
    private String lastName;

    @Column
    private int age;

    @Column
    private String indeks;

//    @OneToMany(cascade = { CascadeType.ALL, CascadeType.REMOVE})
//    @Cascade({org.hibernate.annotations.CascadeType.ALL})
//    private List<Grade> gradeList = new ArrayList<>();

    public Student(String name, String lastName, int age, String indeks) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.indeks = indeks;
    }
}
