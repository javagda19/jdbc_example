package com.javagda19.jdbc;

import com.javagda19.model.Student;
import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // kroki które należy podjąć:
        // 1. wczytać plik konfiguracyjny
        JdbcSettings jdbcSettings = new JdbcSettings("/jdbc.properties");
        System.out.println(jdbcSettings.getJdbcConnectionURL());

        StudentDao studentDao = new StudentDao(jdbcSettings);

        studentDao.createTable();

        Scanner scanner = new Scanner(System.in);

        boolean isWorking = true;

        do{
            String linia = scanner.nextLine();

            if(linia.equalsIgnoreCase("quit")){
                isWorking = false;
            }else{

                String[] slowa = linia.split(" ");

                String komenda = slowa[0];
                if(komenda.equalsIgnoreCase("dodaj")){
                    Student student = new Student(slowa[1], slowa[2], Integer.parseInt(slowa[3]), slowa[4]);
                    studentDao.insertStudent(student);

                    System.out.println("Stworzono studenta: " + student + " o identyfikatorze: " + student.getId());
                }else if(komenda.equalsIgnoreCase("usun")){
                    Integer szukanyIdentyfikator = Integer.parseInt(slowa[1]);

                    studentDao.remove(szukanyIdentyfikator);
                }else if(komenda.equalsIgnoreCase("zmien")){
                    Integer szukanyIdentyfikator = Integer.parseInt(slowa[1]);

                    String newName = slowa[2];
                    String newLastName = slowa[3];
                    String newAge = slowa[4];
                    String newIndex = slowa[5];

                    studentDao.modify(szukanyIdentyfikator, newName, newLastName, Integer.parseInt(newAge), newIndex);
                }else if(komenda.equalsIgnoreCase("wypisz")){
                    Integer szukanyIdentyfikator = Integer.parseInt(slowa[1]);

                    Optional<Student> student = studentDao.getById(szukanyIdentyfikator);
                    if(student.isPresent()) {
                        System.out.println("Student: " + student.get());
                    }else{
                        System.out.println("Student nie został odnaleziony.");
                    }
                }


            }

        }while (isWorking);

    }
}
