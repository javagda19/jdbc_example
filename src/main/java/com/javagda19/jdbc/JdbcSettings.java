package com.javagda19.jdbc;

import com.mysql.cj.jdbc.MysqlDataSource;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Properties;

public class JdbcSettings {

    // klasa properties i jej instancja do przechowania ustawień z pliku
    private Properties jdbcProperties;

    // konstruktor który przyjmuje nazwę pliku i ładuje properties
    public JdbcSettings(String propertiesFileName) {
        this.jdbcProperties = loadProperties(propertiesFileName);
    }

    // Tworzymy metodę do załadowania ustawień z pliku.
    // w metodzie użyjemy sposobu na ładowanie plików z zasobów (resources)
    private Properties loadProperties(String propertiesFileName) {
        try {
            // załaduj plik z 'resources' jako strumień (czyli otwórz plik)
//            InputStream inputStream = new FileInputStream(propertiesFileName); // to rozwiązanie działa dla pliku z dysku
            InputStream inputStream = JdbcSettings.class.getResourceAsStream(propertiesFileName);
            if (inputStream == null) {
                // błąd
                return null;
            }

            Properties properties = new Properties();
            properties.load(inputStream);
            return properties;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getPort() {
        return jdbcProperties.getProperty("jdbc.port");
    }

    private String getHost() {
        return jdbcProperties.getProperty("jdbc.host");
    }

    private String getPassword() {
        return jdbcProperties.getProperty("jdbc.password");
    }

    private String getUsername() {
        return jdbcProperties.getProperty("jdbc.username");
    }

    private String getDatabase() {
        return jdbcProperties.getProperty("jdbc.database");
    }

    public String getJdbcConnectionURL() {
        return "jdbc:mysql://" + getHost() + ":" + getPort() + "/" + getDatabase() + "?serverTimezone=UTC";
    }

    /**
     * Tworzymy obiekt do połączenia z bazą.
     *
     * @return zwraca gotowy obiekt ustawień mysql.
     */
    public MysqlDataSource getMysqlDataSource() /* throws SQLException*/{
        MysqlDataSource mysqlDataSource = new MysqlDataSource();

        mysqlDataSource.setServerName(getHost());
        mysqlDataSource.setPort(Integer.parseInt(getPort()));
        mysqlDataSource.setUser(getUsername());
        mysqlDataSource.setPassword(getPassword());
        mysqlDataSource.setDatabaseName(getDatabase());

        try {
            mysqlDataSource.setUseSSL(false);
            mysqlDataSource.setServerTimezone("UTC");
        } catch (SQLException sqlException) {
            System.err.println("Error, unable to set timezone to UTC.");
        }

        return mysqlDataSource;
    }
}
