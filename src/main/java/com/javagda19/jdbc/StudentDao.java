package com.javagda19.jdbc;

import com.javagda19.model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StudentDao {
    private JdbcSettings jdbcSettings;

    public StudentDao(JdbcSettings jdbcSettings) {
        this.jdbcSettings = jdbcSettings;
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = jdbcSettings.getMysqlDataSource().getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    // przenieść metodę do tworzenia tabeli
    public void createTable() {
        Connection connection = getConnection();

        // nawiązanie połączenia
        String creationString = "create table if not exists students(\n" +
                "    `id` INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,\n" +
                "    `name` VARCHAR(255) NOT NULL,\n" +
                "    `lastName` VARCHAR(255) NOT NULL,\n" +
                "    `age` INTEGER NOT NULL,\n" +
                "    `indeks` VARCHAR(6) NOT NULL\n" +
                ");";

        try (PreparedStatement preparedStatement = connection.prepareStatement(creationString)) {
            boolean result = preparedStatement.execute();

            if (result) {
                System.out.println("SUKCES! Tabela utworzona!"); // ObjectMapper
            } else {
                System.out.println("Brak sukcesu. Błąd :(");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // przenieść metodę do dodawania (insert) rekordu
    public void insertStudent(Student student) {
        Connection connection = getConnection();

        String insertQuery = "insert into `students` values(NULL, ?, ?, ?, ?);";
        try (PreparedStatement preparedStatement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {

            // uzupełniamy zapytanie (znaki zapytania zastępujemy kolejno wartościami)
            preparedStatement.setString(1, student.getName());
            preparedStatement.setString(2, student.getLastName());
            preparedStatement.setInt(3, student.getAge());
            preparedStatement.setString(4, student.getIndeks());

            preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next(); // przechodzimy do pierwszego wiersza,
            // ponieważ domyślnie iterator nie wskazuje na żaden wiersz

            Long resultId = resultSet.getLong(1);
            student.setId(resultId);
            System.out.println("Wygenerowałem studenta z identyfikatorem : " + resultId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // przenieść metodę do pobierania listy studentów
    public List<Student> getAllStudents() {
        Connection connection = getConnection();
        //

        // zapytanie do pobrania wszystkich studentów z bazy
        String selectQuery = "SELECT * FROM students";
        List<Student> studentList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(selectQuery)) {
            ResultSet resultSet = preparedStatement.executeQuery(); //

            while (resultSet.next()) { // dopóki jest następny rekord
                Student student1 = new Student();

                student1.setId(resultSet.getLong(1));
                student1.setName(resultSet.getString(2));
                student1.setLastName(resultSet.getString(3));
                student1.setAge(resultSet.getInt(4));
                student1.setIndeks(resultSet.getString(5));
//                student1.setIndeks(resultSet.getString(6));

                studentList.add(student1);
            }

//            studentList.forEach(System.out::println); // dopisz adnotację @ToString
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return studentList;
    }

    public void remove(Integer szukanyIdentyfikator) {
        Connection connection = getConnection();

        // zapytanie do pobrania wszystkich studentów z bazy
        String removeQuery = "DELETE FROM students where id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(removeQuery)) {

            preparedStatement.setLong(1, szukanyIdentyfikator);

            int result = preparedStatement.executeUpdate();
            System.out.println("Result: " + result);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void modify(Integer szukanyIdentyfikator, String newName, String newLastName, int parseInt, String newIndex) {
        Connection connection = getConnection();

        String updateQuery = "UPDATE `students` set `name` = ?, `lastname` = ?, `age` = ?, `indeks` = ? where id = ?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(updateQuery)) {

            // uzupełniamy zapytanie (znaki zapytania zastępujemy kolejno wartościami)
            preparedStatement.setString(1, newName);
            preparedStatement.setString(2, newLastName);
            preparedStatement.setInt(3, parseInt);
            preparedStatement.setString(4, newIndex);
            preparedStatement.setLong(5, szukanyIdentyfikator);

            int modified = preparedStatement.executeUpdate();

            System.out.println("Zmodyfikowałem: " + modified);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Optional<Student> getById(Integer szukanyIdentyfikator) {
        Connection connection = getConnection();

        String selectQuery = "SELECT * FROM students where id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(selectQuery)) {
            preparedStatement.setLong(1, szukanyIdentyfikator);

            ResultSet resultSet = preparedStatement.executeQuery(); //

            if (resultSet.next()) { // dopóki jest następny rekord
                Student student1 = new Student();

                student1.setId(resultSet.getLong(1));
                student1.setName(resultSet.getString(2));
                student1.setLastName(resultSet.getString(3));
                student1.setAge(resultSet.getInt(4));
                student1.setIndeks(resultSet.getString(5));
//                student1.setIndeks(resultSet.getString(6));

                return Optional.of(student1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();

    }
}





