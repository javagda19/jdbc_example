package com.javagda19.hibernate;

import com.javagda19.model.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StudentDao {

    public void insert(Student student) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            session.saveOrUpdate(student);

            transaction.commit();
        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public List<Student> getStudentList() {
        List<Student> list = new ArrayList<>();

        // z językiem hql
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            Query<Student> query = session.createQuery("from Student", Student.class);

            list.addAll(query.list()); // wynik zapytania
        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            System.err.println("Error: " + sqle.getMessage());
        }

        return list;
    }

    public List<Student> getStudentList2() {
        List<Student> list = new ArrayList<>();

        // bez języka hql
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();

            // zapytanie na podstawie criteria buildera o CriteriaQuery
            CriteriaQuery<Student> studentQuery = builder.createQuery(Student.class);

            // z obiektu root możemy pobrać wartości kolumn.
            // tworzymy go żeby mówić w jakiej tabeli szukamy
            Root<Student> studentRoot = studentQuery.from(Student.class);

            // szukam studentów w tabeli studentów
            studentQuery.select(studentRoot);

            return session.createQuery(studentQuery).getResultList();
        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            System.err.println("Error: " + sqle.getMessage());
        }

        return list;
    }

    public Optional<Student> getStudentById(long searchedIdentifier) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            Query<Student> query = session.createQuery("from Student where id = :id", Student.class);
            query.setParameter("id", searchedIdentifier);

            Student student = query.getSingleResult();
            return Optional.of(student);
        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            System.err.println("Error: " + sqle.getMessage());
        }

        return Optional.empty();
    }

    public Optional<Student> getStudentById2(long searchedIdentifier) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();

            // zapytanie na podstawie criteria buildera o CriteriaQuery
            CriteriaQuery<Student> studentQuery = builder.createQuery(Student.class);

            // z obiektu root możemy pobrać wartości kolumn.
            // tworzymy go żeby mówić w jakiej tabeli szukamy
            Root<Student> studentRoot = studentQuery.from(Student.class);

            // szukam studentów w tabeli studentów
            studentQuery.select(studentRoot).where(builder.equal(studentRoot.get("id"), searchedIdentifier));

            return Optional.of(session.createQuery(studentQuery).getSingleResult());
        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            System.err.println("Error: " + sqle.getMessage());
        }

        return Optional.empty();
    }

    public Optional<Student> getStudentByName(String name, String lastname) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();

            // zapytanie na podstawie criteria buildera o CriteriaQuery
            CriteriaQuery<Student> studentQuery = builder.createQuery(Student.class);

            // z obiektu root możemy pobrać wartości kolumn.
            // tworzymy go żeby mówić w jakiej tabeli szukamy
            Root<Student> studentRoot = studentQuery.from(Student.class);

            // szukam studentów w tabeli studentów
            // Spring boot specification
            studentQuery.select(studentRoot)
                    .where(builder.and(
                            builder.equal(studentRoot.get("name"), name),
                            builder.equal(studentRoot.get("lastName"), lastname))
                    );

            return Optional.of(session.createQuery(studentQuery).getSingleResult());
        } catch (NoResultException sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            System.err.println("Error: " + sqle.getMessage());
//            sqle.printStackTrace();
        }

        return Optional.empty();
    }

    public void removeStudentWithId(Long id) {
        Optional<Student> student = getStudentById(id);
        if (student.isPresent()) {
            Student student1 = student.get();

            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

            Transaction transaction = null;
            try (Session session = sessionFactory.openSession()) {
                transaction = session.beginTransaction();

                session.remove(student1);

                transaction.commit();
            } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
                if (transaction != null) {
                    transaction.rollback();
                }
            }
        }
    }

    public void remove(List<Student> list){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            for(Student student : list) {
                session.remove(student);
            }

            transaction.commit();
        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public void updateStudent(int identifier, String newName, String newLastname, int age, String indeks) {
        // 1. znalezc studenta o id identifier
        Optional<Student> foundStudent = getStudentById(identifier);
        if (foundStudent.isPresent()) {
            // 2. aktualizować studenta
            Student modified = foundStudent.get();
            modified.setName(newName);
            modified.setLastName(newLastname);
            modified.setAge(age);
            modified.setIndeks(indeks);

            updateStudent(modified);
        }
    }

    private void updateStudent(Student student) {
        insert(student);
    }


    // operacja zapisu - metoda - save / saveOrUpdate
    //

}
