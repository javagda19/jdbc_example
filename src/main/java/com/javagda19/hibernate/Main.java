package com.javagda19.hibernate;

import com.javagda19.model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        StudentDao studentDao = new StudentDao();
        Scanner scanner = new Scanner(System.in);

        boolean isWorking = true;

        List<Student> studentList = new ArrayList<>();
        studentList.add(studentDao.getStudentById(5).get());
        studentList.add(studentDao.getStudentById(2).get());
        studentList.add(studentDao.getStudentById(3).get());
        studentList.add(studentDao.getStudentById(4).get());
        studentDao.remove(studentList);

        do{
            String linia = scanner.nextLine();

            if(linia.equalsIgnoreCase("quit")){
                isWorking = false;
            }else{

                String[] slowa = linia.split(" ");

                String komenda = slowa[0];
                if(komenda.equalsIgnoreCase("dodaj")){
                    Student student = new Student(slowa[1], slowa[2], Integer.parseInt(slowa[3]), slowa[4]);
                    studentDao.insert(student);

                    System.out.println("Stworzono studenta: " + student + " o identyfikatorze: " + student.getId());
                }else if(komenda.equalsIgnoreCase("usun")){
                    Long szukanyIdentyfikator = Long.parseLong(slowa[1]);

                    studentDao.removeStudentWithId(szukanyIdentyfikator);
                }else if(komenda.equalsIgnoreCase("zmien")){
                    Integer szukanyIdentyfikator = Integer.parseInt(slowa[1]);

                    String newName = slowa[2];
                    String newLastName = slowa[3];
                    String newAge = slowa[4];
                    String newIndex = slowa[5];

                    studentDao.updateStudent(szukanyIdentyfikator, newName, newLastName, Integer.parseInt(newAge), newIndex);
                }else if(komenda.equalsIgnoreCase("wypisz")){
                    Integer szukanyIdentyfikator = Integer.parseInt(slowa[1]);

                    Optional<Student> student = studentDao.getStudentById2(szukanyIdentyfikator);
                    if(student.isPresent()) {
                        System.out.println("Student: " + student.get());
                    }else{
                        System.out.println("Student nie został odnaleziony.");
                    }
                }


            }

        }while (isWorking);

        HibernateUtil.getSessionFactory().close();
    }
}
